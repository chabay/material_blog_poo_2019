<?php
/*
  ./app/routeurs/posts.php
*/


switch ($_GET['posts']) {
  case 'show':
    /*
    ROUTE DETAIL D'UN POST
    PATTERN: posts/id/slug
    CTRL: PostsControleur
    ACTION: show
    */
    $ctrl = new \App\Controleurs\PostsControleur();
    $ctrl->showAction($_GET['id']);
  break;

}
