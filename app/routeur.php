<?php
/*
  ./app/routeur.php
*/

/*
ROUTE DETAIL D'UN POST
PATTERN: posts/id/slug
CTRL: PostsControleur
ACTION: show
*/

if(isset($_GET['posts'])):
  include_once '../app/routeurs/posts.php';

elseif(isset($_GET['categories'])):
  include_once '../app/routeurs/categories.php';

else:
  $ctrl = new \App\Controleurs\PostsControleur();
  $ctrl->indexAction();

endif;
