<?php
/*
  ./app/controleurs/CategoriesGestionnaire.php
*/
namespace App\Modeles;

class CategoriesGestionnaire extends \Noyau\Classes\GestionnaireGenerique {
  public function __construct(){
    $this->_table = 'categories';
    $this->_class = '\App\Modeles\Categorie';
  }
}
