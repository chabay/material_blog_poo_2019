<?php
/*
  ./app/controleurs/PostsGestionnaire.php
*/
namespace App\Modeles;
use \Noyau\Classes\App;

class PostsGestionnaire extends \Noyau\Classes\GestionnaireGenerique {
  public function __construct(){
    $this->_table = 'posts';
    $this->_class = '\App\Modeles\Post';
  }

  public function findOneById(int $id = 1) {
    $sql = "SELECT *, posts.id AS postID, auteurs.id AS auteurID
            FROM posts
            JOIN auteurs ON auteur = auteurs.id
            WHERE posts.id = :id;";

    $rs = App::getConnexion()->prepare($sql);
    $rs->bindValue(':id', $id, \PDO::PARAM_INT);
    $rs->execute();

    $tableau = $rs->fetch(\PDO::FETCH_ASSOC);
    $post = new Post($tableau);
    $post->setId($tableau['postID']);
    $post->setAuteurPseudo($tableau['pseudo']);
    return $post;
  }

  public function findAll(array $data) {
    $sql = "SELECT *, posts.id AS postID, auteurs.id AS auteurID
            FROM posts
            JOIN auteurs ON auteur = auteurs.id
            ORDER BY datePublication DESC
            LIMIT 5;";

    $rs = App::getConnexion()->query($sql);

    $tableaux = $rs->fetchAll(\PDO::FETCH_ASSOC);

      $tab = [];
      foreach ($tableaux as $tableau) {
        $post = new Post($tableau);
        $post->setId($tableau['postID']);
        $post->setAuteurPseudo($tableau['pseudo']);
        $tab[] = $post;
      }
      return $tab;

  }
}
