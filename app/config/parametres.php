<?php

// PARAMETRES DE CONNEXION A LA DB
  define('DB_HOST', 'localhost:3306');
  define('DB_NAME', 'material_blog');
  define('DB_USER', 'root');
  define('DB_PWD' , 'root');

// INITIALISATION DES ZONES DYNAMIQUES
   $zones = ['title', 'content1'];

// AUTRES CONSTANTES
define('PUBLIC_FOLDER', 'public');
define('ADMIN_FOLDER', 'admin');

// CONSTANTES
  define('POST_INDEX_TITLE', "Latest posts");
