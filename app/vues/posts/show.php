<?php
/*
  ./app/vues/posts/show.php
  Variables disponibles :
  - $post POST(titre, slug, texte, pseudo, datePublication)
*/

use Noyau\Classes\Template;
?>


<!-- Blog Post -->
<?php
      Template::startZone();
      echo $post->getTitre();
      Template::stopZone('title');
?>
<!-- Title -->
<?php Template::startZone(); ?>
<h1><?php echo $post->getTitre(); ?></h1>

<!-- Author -->
<p class="lead">
by <a href="#"><?php echo $post->getAuteurPseudo(); ?></a>
</p>

<hr>

<!-- Date/Time -->
<p>Posted on
<?php echo \Noyau\Classes\Utils::formater_date($post->getDatePublication()); ?></p>

<hr>

<!-- Preview Image -->
<!-- <img class="img-responsive z-depth-2" src="http://lorempixel.com/750/390/technics/2" alt=""> -->

<hr>

<!-- Post Content -->
<div><?php echo $post->getTexte(); ?></div>


<hr>
<?php Template::stopZone('content1'); ?>
