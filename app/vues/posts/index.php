<?php
/*
  ./app/vues/posts/index.php
  Variables disponibles :
  - $posts array(POST(id, titre, slug, texte, media, auteur, datePublication))
*/
use Noyau\Classes\Template;
?>
<?php
      Template::startZone();
      echo POST_INDEX_TITLE;
      Template::stopZone('title');
?>
<?php Template::startZone(); ?>
<h1 class="page-header">
    Material Design for Bootstrap
    <small>made with love</small>
</h1>

<?php foreach ($posts as $post): ?>
<h2>
    <a href="posts/<?php echo $post->getId(); ?>/<?php echo $post->getSlug(); ?>"><?php echo $post->getTitre(); ?></a>
</h2>
<p class="lead">
  by <a href="#"><?php echo $post->getAuteurPseudo(); ?></a>
</p>
<p> Posted on
  <?php echo \Noyau\Classes\Utils::formater_date($post->getDatePublication()); ?>   </p>
<hr>
<!--
<img class="img-responsive z-depth-2"src="http://lorempixel.com/750/390/technics/2" alt="">
-->
<hr>
   <div><?php echo \Noyau\Classes\Utils::tronquer($post->getTexte()); ?></div>
<a href="posts/<?php echo $post->getId(); ?>/<?php echo $post->getSlug(); ?>">
  <button type="button" class="btn btn-info waves-effect waves-light">Read more</button>
</a>
<hr>
<?php endforeach; ?>
<?php Template::stopZone('content1'); ?>
