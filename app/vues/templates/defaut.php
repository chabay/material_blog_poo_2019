<?php
/*
  ./app/vues/templates/defaut.php
 */
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <?php include '../app/vues/templates/partials/_head.php'; ?>
</head>

<body>
  <!-- Navigation -->
  <?php include '../app/vues/templates/partials/_nav.php'; ?>

  <!-- Page Content -->
  <div class="container">

    <div class="row">
      <!-- Blog Entries Column -->
      <div class="col-md-8">
        <?php echo \Noyau\Classes\Template::getZone('content1'); ?>
        <!--<?php //echo $content1; ?>-->
      </div>
      <!-- Blog Sidebar Widgets Column -->
      <div class="col-md-4">

        <?php
          $ctrl = new \App\Controleurs\CategoriesControleur();
          $ctrl->indexAction(['orderByField' => 'titre', 'orderBySens' => 'ASC']);
        ?>

      </div>
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->


  <!-- Footer -->
  <?php include '../app/vues/templates/partials/_footer.php'; ?>



  <!-- SCRIPTS -->
<?php include '../app/vues/templates/partials/_scripts.php'; ?>


</body>

</html>
