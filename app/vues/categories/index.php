<?php
/*
  ./app/vues/categories/index.php
  Variables disponibles :
  - $categories array(CATEGORIE(id, titre, slug))
*/
?>
<ul>
  <?php foreach ($categories as $categorie): ?>
    <li><a href="categories/<?php echo $categorie->getId(); ?>/<?php echo $categorie->getSlug(); ?>"><?php echo $categorie->getTitre(); ?></a></li>
  <?php endforeach; ?>
</ul>
