<?php
/*
  ./app/vues/categories/show.php
  Variables disponibles :
  - $categorie array(id, titre, slug)
*/

use Noyau\Classes\Template;
?>

<?php Template::startZone(); ?>
<h1><?php echo $categorie->getTitre(); ?></h1>
<?php Template::stopZone('content1'); ?>
