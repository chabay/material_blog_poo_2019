<?php
/*
  ./www/index.php
 */
//Chargement des fichiers d'initialisation
 require_once '../app/config/parametres.php';
 require_once '../vendor/autoload.php';

//Démarrage de l'application
 \Noyau\Classes\App::start();

 include_once '../app/routeur.php'; // Hydrater $zone1 grâce à $connexion

  // On ne charge le template QUE SI on n'est PAS EN AJAX !!!
  if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH'])
          && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')) {
            require_once '../app/vues/templates/defaut.php'; // on affiche $zone1
  }

//Fin de l'application
 \Noyau\Classes\App::close();
