<?php
/*
 ./noyau/classes/Template.php
 */
namespace Noyau\Classes;

abstract class Template {

  public static $_zones;

  public static function addZones(array $zones) {
    SELF::$_zones = [];
    foreach ($zones as $zone):
      SELF::$_zones[$zone] = '';
    endforeach;
  }

  private static function setZone(string $zone, string $contenu) {
    SELF::$_zones[$zone] = $contenu;
  }

  public static function startZone(){
    ob_start();
  }

  public static function stopZone(string $zone){
    SELF::setZone($zone, ob_get_clean());
  }

  public static function getZone(string $zone) {
    return SELF::$_zones[$zone];
  }


}
