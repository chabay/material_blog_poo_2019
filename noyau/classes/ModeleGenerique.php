<?php
/*
 ./noyau/classes/ModeleGenerique.php
 */
namespace Noyau\Classes;

abstract class ModeleGenerique {

  //Constructeur
  public function __construct(array $data = null) {
    if($data):
      $this->hydrater($data);
    endif;
  }
  public function __destruct() {
  }


  public function hydrater(array $data = null) {
    if($data):
      foreach ($data as $nomProp => $valeur) {
        $nomMethode = 'set' . ucfirst($nomProp);
      if(method_exists($this, $nomMethode)):
        $this->$nomMethode($valeur);
      endif;
      }
    endif;
  }
}
