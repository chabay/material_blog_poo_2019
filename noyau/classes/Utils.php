<?php

namespace Noyau\Classes;

abstract class Utils {

  public static function formater_date(string $date, string $format = 'D d M Y') :string {
    return date_format(date_create($date), $format);
  }

  public static function tronquer(string $chaine, int $nbreCaracteres = 200) :string {
    if(strlen($chaine) > $nbreCaracteres):
      $positionEspace = strpos($chaine, ' ', $nbreCaracteres);
      return substr($chaine, 0, $positionEspace);
    else:
      return $chaine;
    endif;
  }


}
