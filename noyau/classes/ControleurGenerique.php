<?php
/*
 ./noyau/classes/ControleurGenerique.php
 */
namespace Noyau\Classes;

abstract class ControleurGenerique {
  protected $_gestionnaire, $_table;

  public function __construct() {
    $gestionnaireName = '\App\Modeles\\' . ucfirst($this->_table) . 'Gestionnaire';
    $this->_gestionnaire = new $gestionnaireName();
  }

  public function showAction($data, string $field = 'id'){
    $r = substr($this->_table, 0, -1);
    $methodName = 'findOneBy' . ucfirst($field);
    $$r = $this->_gestionnaire->$methodName($data);

    include '../app/vues/' . $this->_table . '/show.php';

  }

  public function indexAction(array $userData = []){
    $defaultValues = [
      'view' => 'index',
      'orderByField' => 'id',
      'orderBySens' => 'ASC',
      'limit' => false,
      'offset' => 0
    ];
    $data = array_merge($defaultValues, $userData);
    $r = $this->_table;
    $$r = $this->_gestionnaire->findAll($data);

    include '../app/vues/' . $this->_table . '/' . $data['view'] . '.php';

  }


}
