<?php
/*
 ./noyau/classes/GestionnaireGenerique.php
 */
namespace Noyau\Classes;

abstract class GestionnaireGenerique {
  protected $_table, $_class;

  //Méthodes CRUD

  public function findOneById(int $id){
    $sql = "SELECT *
            FROM `{$this->_table}`
            WHERE id = :id;";
    $rs = App::getConnexion()->prepare($sql);
    $rs->bindValue(':id', $id, \PDO::PARAM_INT);
    $rs->execute();
    return new $this->_class($rs->fetch(\PDO::FETCH_ASSOC));
  }

  public function findAll(array $data){
    if($data['limit']):
      $sql = "SELECT *
            FROM `{$this->_table}`
            ORDER BY `{$data['orderByField']}` {$data['orderBySens']}
            LIMIT :limit
            OFFSET :offset;";
    else:
      $sql = "SELECT *
              FROM `{$this->_table}`
              ORDER BY `{$data['orderByField']}` {$data['orderBySens']};";
    endif;

    $rs = App::getConnexion()->prepare($sql);

    if($data['limit']):
      $rs->bindValue(':limit', $data['limit'], \PDO::PARAM_INT);
      $rs->bindValue(':offset', $data['offset'], \PDO::PARAM_INT);
    endif;

    $rs->execute();
    $tab = $rs->fetchAll(\PDO::FETCH_ASSOC);
    return $this->fromAssocToObj($tab);
  }


  //Autres Méthodes
  protected function fromAssocToObj(array $rs) {
    $tab = [];
    foreach ($rs as $r) {
      $tab[] = new $this->_class($r);
    }
    return $tab;
  }



}
